const gameContainer = document.getElementById("game");
const spanElement = document.getElementById("span-element");
const pairElement = document.getElementById("span-pair-element");
const outerSection = document.getElementById("outer-section");
const startButton = document.getElementById("start-btn");
const restartButton = document.getElementById("restart-btn");
const timeElement = document.getElementById("time-span-element");
const bestScore = document.getElementById("best-score-element");
const cardCountElement = document.getElementById("card-count");
const errorMsgElement = document.getElementById("error-message");
const playNowButton = document.getElementById("play-now");
const bgElement = document.getElementById("background-container");
const showInstructionsButton = document.getElementById("show-instructions");
const closePopupButton = document.getElementById("close-popup");
const instructionsPopup = document.getElementById("instructions-popup");
const restartPopupElement = document.getElementById("restart-popup");
const noBtnElement = document.getElementById("no-btn");
const yesBtnElement = document.getElementById("yes-btn");
const completePopup = document.getElementById("complete-popup")
const completeCloseElement = document.getElementById("complete-close")

outerSection.style.display = "none";

playNowButton.addEventListener("click", () => {
  outerSection.style.display = "block";
  bgElement.style.display = "none";
})

showInstructionsButton.addEventListener("click", () => {
  instructionsPopup.style.display = "block";
});

closePopupButton.addEventListener("click", () => {
  instructionsPopup.style.display = "none";
});


const COLORS = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif"
]

let color_half_length = COLORS.length / 2;

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;
  //console.log(counter);

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);
    //console.log(index);
    // Decrease counter by 1
    counter--;
    //console.log(counter);

    // And swap the last element with it
    let temp = array[counter];
    //console.log(temp);
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

//let shuffledColors = shuffle(COLORS);
//console.log(shuffledColors);


const cards = [];
// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add("card");
    //newDiv.style.backgroundColor = "gray";

    //stored the color as a data attribute on the card element
    newDiv.setAttribute("dataColor", color);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);

    cards.push(newDiv);
  }
}

let previouscard = "";
let cardsOpen = 0;
let timeInterval = undefined;
let selectedCardCount;

//start button functionality
startButton.classList.add("change");

startButton.addEventListener("click", () => {
  startButton.classList.remove("change");
  restartButton.classList.add("change");

  selectedCardCount = parseInt(cardCountElement.value);
  let cardCount;
  if (selectedCardCount <= 1) {
    errorMsgElement.textContent = "Provide a number between 2 to 24!"
    return;
  }

  errorMsgElement.textContent = "";

  if (selectedCardCount % 2 === 0) {
    cardCount = selectedCardCount;
  } else {
    cardCount = selectedCardCount + 1;
  }
  const selectedColors = COLORS.slice(0, cardCount / 2);
  console.log(selectedColors);

  const shuffledColors = shuffle([...selectedColors, ...selectedColors]);

  gameContainer.textContent = "";
  createDivsForColors(shuffledColors);

  previouscard = "";
  cardsOpen = 0;

  if (timeInterval === undefined) {
    timeInterval = setInterval(() => {
      timeElement.textContent = parseInt(timeElement.textContent) + 1;
    }, 1000)
  }
});

//restart button functionality

restartButton.addEventListener("click", () => {
  //instructionsPopup.style.display = "block";
  restartButton.classList.remove("change");
  //window.location.href = "index.html";
  restartPopupElement.style.display = "block";
  restartPopupElement.style.zIndex = "2";

  noBtnElement.addEventListener("click", () => {
    restartButton.classList.add("change");
    restartPopupElement.style.display = "none"
    startButton.classList.remove("change");
  })

  yesBtnElement.addEventListener("click", () => {
    window.location.href = "index.html";
  })
})

let bestScoreValue = parseInt(localStorage.getItem(`bestscore-${selectedCardCount}`));
console.log(bestScoreValue);

if (isNaN(bestScoreValue)) {
  bestScoreValue = Infinity;
  localStorage.setItem("bestscore", bestScoreValue);
} else {
  bestScoreValue = parseInt(bestScoreValue);
  bestScore.textContent = bestScoreValue;
}

// handler function logic

function handleCardClick(event) {
  if (!startButton.classList.contains("change") && cardsOpen < 2) {
    if (previouscard === "") {
      cardsOpen += 1;
      previouscard = event.target;
      flipCard(event.target);
      spanElement.textContent = parseInt(spanElement.textContent) + 1;
    } else if (checkCardsMatch(event.target, previouscard)) {

      flipCard(event.target);
      previouscard = "";
      cardsOpen = 0;
      pairElement.textContent = parseInt(pairElement.textContent) + 1;

      // for pair count match:
      let pair_count;
      if (selectedCardCount % 2 === 0) {
        pair_count = selectedCardCount / 2;
        console.log(pair_count);
      } else {
        pair_count = Math.floor(selectedCardCount / 2) + 1;
        console.log(pair_count);
      }

      if (pairElement.textContent == pair_count) {
        if (parseInt(spanElement.textContent) < bestScoreValue) {
          bestScoreValue = parseInt(spanElement.textContent);
          localStorage.setItem(`bestscore-${selectedCardCount}`, bestScoreValue);
          bestScore.textContent = bestScoreValue;
          console.log(bestScoreValue);
          startButton.classList.add("change");
        }

        clearInterval(timeInterval);
        timeInterval = undefined;

        completePopup.style.display = "block";
        completePopup.style.zIndex = "3";

        completeCloseElement.addEventListener("click", () => {
          completePopup.style.display = "none"
          console.log("clicked");
        })
      }

    } else {
      cardsOpen += 1;
      flipCard(previouscard)
      flipCard(event.target)
      spanElement.textContent = parseInt(spanElement.textContent) + 1;
      setTimeout(() => {
        cardsOpen = 0;
        returnCardOriginalPosition(event.target);
        returnCardOriginalPosition(previouscard);
        previouscard = "";
      }, 1000)
    }
  }
}

function checkCardsMatch(currentCard, previouscard) {
  return currentCard.getAttribute("dataColor") === previouscard.getAttribute("dataColor");
}

function flipCard(card) {
  card.classList.add("rotate");
  card.style.backgroundImage = `url(${card.getAttribute("dataColor")})`
  card.style.backgroundSize = "cover";
}

function returnCardOriginalPosition(card) {
  card.classList.remove("rotate");
  card.style.backgroundImage = "";
  card.style.backgroundColor = "#94ADD7";
}